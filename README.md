Mémo pour le travail de groupe
==============================
Auteur           | Date
---------------- | ----------------
Baeriswyl Julien | 2016-12-14

Voici quelques remarques / suggestions concernant le travail de groupe.  
Elles sont pour la plupart implicites et permettent de travailler efficacement
et gagner du temps pour tout le monde.  

*Les remarques ou sections en italique sont absentes de l'e-mail précédemment envoyé.*  

## Prérequis avant travail ##
+ Lisez soigneusement les consignes et données de projet avant de débuter une session de travail.  
  Cela permet aux collaborateurs de se mettre rapidement d'accord sur l'organisation et de travailler directement.  
  Autrement dit, arrivez en étant opérationnel et _prêt-à-l'emploi_.  

## *Concernant les priorités* ##
+ Le travail de groupe implique de plus grandes responsabilités que le travail personnel.  
  En effet, les responsabilités qui vous incombent concernent également vos collaborateurs.  
  Le travail de groupe est donc prioritaire sur le travail personnel.  
+ Inutile de faire tout le projet par vous-même. Concentrez-vous sur vos tâches.  
  Cela sera bénéfique pour toute l'équipe.  
  Puis, si pour des raisons de compréhension, vous souhaitez refaire le projet à un niveau personnel,  
  libre à vous de le faire, tant que cela ne nuit pas au rendu collaboratif.

## Concernant les rôles et tâches ##
+ Vos collaborateurs ne peuvent pas deviner quelles sont vos compétences et difficultés.  
  Par conséquent, il est essentiel de les annoncer dès le début, avant même d'attribuer les tâches.  
  Il faut également ôter tout doute ou problème éventuel avant de commencer à travailler.  
+ Une organisation au sein du groupe est utile, qu'elle soit hiérarchique ou non.  
+ Si un superviseur est choisi, tenez compte du fait que son rôle lui apporte du travail supplémentaire.  
  En effet, il doit s'assurer que les résultats et objectifs soit atteints dans le temps imparti.  

## Concernant l'organisation temporelle ##
+ Vos collaborateurs dépendent directement de votre travail.  
  Ne faîtes donc pas votre travail au dernier moment, car cela incapacitera toute l'équipe.  
  Un travail fait tôt est synonyme d'absence de stress et soucis.  
+ Si vous avez fait votre travail, il n'est pas possible pour vos collaborateurs de le deviner (et vice-versa).  
  N'attendez donc pas pour leurs remettre vos résultats.  
+ Si vous êtes dans l'incapacité totale de faire votre travail, dites-le immédiatement  
  et ne laissez vos collaborateurs dans l'ignorance de votre situation.  
+ Si vous êtes dans l'incapacité de venir à une séance, annoncez-le le plus rapidement possible  
  à vos collaborateurs, afin qu'ils puissent s'organiser sans vous.  
+ Tenez-vous régulièrement à jour quant au travail effectué par vos collaborateurs.  
  Cela permet de déterminer ce qu'il reste à faire.  
+ Restez joignable et consultez régulièrement vos e-mails, etc.  

## Concernant la qualité du travail ##
+ Par respect pour vos collaborateurs, lisez et respectez les consignes du projet,  
  de manière à produire un travail viable.  
  Le fait de devoir refaire le travail pour cause de non-respect des consignes est un gaspillage de temps.  
  Ne fournissez donc pas votre travail, si celui-ci ne respecte pas les consignes.  
+ Un travail bien fait est synonyme de satisfaction et de sommeil profond.  
+ Le fait d'avoir un résultat n'est pas synonyme de victoire.  
  Remettez en questions les résultats, qu'ils viennent de chez vous ou de vos collaborateurs.  
+ N'hésitez pas à suggérer, discuter et confronter les solutions.  
  Cela augmente les chances de fournir de bonnes solutions et permet de progresser plus vite,  
  avec pour bénéfice un gain de cadence de travail lors des prochains projets.  
+ Afin de produire un travail cohérent, il peut être bénéfique de définir des  
  infrastructures / environnements (outils, ...) communs aux collaborateurs.  

## Concernant les interactions entre collaborateurs ##
+ Soyez communicatif et réactif. Un collaborateur ignoré est un collaborateur inefficace et dubitatif.  

## Concernant l'état d'esprit ##
+ Ne voyez pas votre travail comme une corvée, mais comme un défi.  
  Comme ça, vous motiverez aussi le reste de l'équipe, qui vous fera confiance.  
  Investissez-vous.  
+ *Supprimez "incapable", "je ne sais pas" et "je ne peux pas" de votre vocabulaire.*  
+ *Soyez curieux. Cela vous permettra de trouver des solutions originales et vous ouvrira de nouveaux horizons.*  
+ *Ayez l'esprit d'initiative et n'attendez pas qu'on vous donne du travail.*  
+ *Soyez prévoyant et anticipez les besoins du projets.*  
+ Par respect pour vos collaborateurs, ne visez pas le strict minimum.  
+ *Soyez autonome. Dans la majorité des cas, une documentation existe pour chacun des outils utilisés.*  
  *Donc si vous ne savez pas comment faire quelque chose, n'attendez pas que quelqu'un vous donne la solution.*  
  *Cherchez-la par vous-même. En effet, en entreprise, personne ne vous dira comment résoudre un nouveau problème.*  

### *Lorsque vous bloquez sur un problème* ###
1. N'ayez pas peur, ne paniquez pas. Rappelez-vous que toutes informations nécessaires sont à votre portée.  
2. La résolution d'un problème se base sur 2 étapes fondammentales:  
	+ Analyse du problème (donnée, ressources du projet)  
	+ Synthèse de la solution  
3. Parcourrez toutes les informations à votre disposition et tirez des déductions.  
4. Si votre solution ne fonctionne pas, remettez-la en question.  
   Il se peut que votre analyse soit incorrecte.  
5. Si votre méthode d'analyse est hasardeuse ou prend trop de temps, changez-la.  
   Voir le problème sous un autre angle peut vous aider.  
   Par exemple, partez de là où se trouve le problème et remontez la source d'information (comme un saumon).  

### *Fonctionnement de `Git`* ###
1. `Git` est un `Source Code Manager (SCM)` avec une structure centralisé autour d'un dépôt commun  
   sur un serveur (*remote*) qui fait office de référence.  
   Dans notre cas, le serveur est possédé par __Atlassian__ qui fournit le service __BitBucket__.  
2. Les dépôts locaux sur vos machines sont des clones et ne sont pas synchronisé.  
   Autrement dit, si une modification est faite sur le serveur, elle n'est pas automatiquement reportée sur vos dépôts locaux.  
   Il en va de même pour le dépôt central.  
   En effet, si vous faite des modifications, celles-ci ne sont pas automatiquement reportées sur le dépôt central.  
3. Séquence des opérations lorsque vous travaillez (dépôt déjà cloné):  
   `pull` - travail - tests/contrôles qualité - `add` - `commit` - `push`  
4. Quelques opérations utiles:  

	 Fonction                            | Commande
	 ----------------------------------- | ---------------------------------------
	 Clone local                         | `git clone <url>`
	 Ajouter un fichier                  | `git add <filepath>`
	 Récupérer des fichiers (remote)     | `git checkout <path>`
	 Enregistrer les changements         | `git commit -m "<msg>"`
	 Consulter les branches disponibles  | `git branch`
	 Etat des fichiers (modif, ...)      | `git status`
	 Mise à jour (serveur -> local)      | `git fetch` & `git merge`, ou `git pull` (si pas de branches)
	 Mise à jour (local -> serveur)      | `git push`
